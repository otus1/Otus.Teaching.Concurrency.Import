using FakeItEasy;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using WebApi.Controllers;

namespace WebApiTest
{
    public class Tests
    {

        [Test]
        [Description("�������� ������������ �� Id. �������")]
        public void CanGetUserById_Ok()
        {
            var repo = A.Fake<ICustomerRepository>();

            var customer = new Customer(1, "������", "Test@email.ru", "8-926-111-22-33");
            A.CallTo(() => repo.GetCustomerByIdAsync(A<int>._)).Returns(customer);

            var userCOntroller = new UserController(repo);
            var result = userCOntroller.Get(1).GetAwaiter().GetResult();
            var resultCode = result as ObjectResult;
            Assert.NotNull(result);
            Assert.NotNull(resultCode);
            Assert.True(resultCode is OkObjectResult);
            Assert.AreEqual(StatusCodes.Status200OK, resultCode.StatusCode);
        }

        [Test]
        [Description("�������� ������������ �� Id. ������. �� �������")]
        public void CanGetCustomerById_NotFound()
        {
            var repo = A.Fake<ICustomerRepository>();

            var userController = new UserController(repo);
            var result = userController.Get(-3).GetAwaiter().GetResult();
            var resultCode = result as ObjectResult;
            Assert.NotNull(result);
            Assert.NotNull(resultCode);
            Assert.True(resultCode is NotFoundObjectResult);
            Assert.AreEqual(StatusCodes.Status404NotFound, resultCode.StatusCode);
        }

        [Test]
        [Description("������� ������������. �������")]
        public void CanPostCustomer_Ok()
        {
            var repo = A.Fake<ICustomerRepository>();

            var customer = new Customer(-7, "������", "Test@email.ru", "8-926-111-22-33");
            A.CallTo(() => repo.CheckCustomer(A<Customer>._)).Returns(false);

            var userController = new UserController(repo);
            var result = userController.Post(customer).GetAwaiter().GetResult();
            var resultCode = result as ObjectResult;
            Assert.NotNull(result);
            Assert.NotNull(resultCode);
            Assert.True(resultCode is OkObjectResult);
            Assert.AreEqual(StatusCodes.Status200OK, resultCode.StatusCode);
        }

        [Test]
        [Description("������� ������������. ������: ������������ ����������")]
        public void CanPostCustomer_Error()
        {
            var repo = A.Fake<ICustomerRepository>();

            var customer = new Customer(1, "������", "Test@email.ru", "8-926-111-22-33");
            A.CallTo(() => repo.CheckCustomer(A<Customer>._)).Returns(true);

            var userCOntroller = new UserController(repo);
            var result = userCOntroller.Post(customer).GetAwaiter().GetResult();
            var resultCode = result as ObjectResult;
            Assert.NotNull(result);
            Assert.NotNull(resultCode);
            Assert.True(resultCode is ConflictObjectResult);
            Assert.AreEqual(StatusCodes.Status409Conflict, resultCode.StatusCode);
        }
    }
}