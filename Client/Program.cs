﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Net.Http;
using Flurl;
using System.Text;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace WebClient
{
    class Program
    {
        static string ApiUrl;
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();

            ApiUrl = builder["ApiUrl"];


            var cutomerJson = GetNewCustomer();
            SendCustomer(cutomerJson);
            FindCustomer();

            Console.ReadKey();
        }
        static  string GetNewCustomer()
        {
            Console.Write($"Создание пользователя. Укажите ID: ") ;
            int.TryParse(Console.ReadLine(), out int customerId);
            var customer = new ManualJsonGenerator(customerId, "");
            customer.GenerateAsync();
            Console.WriteLine($"Пользователь: {customer.ToString()}");
            return customer.ManualJsonCutomer;
        }

        static async void SendCustomer(string customer)
        {
            Console.WriteLine($"Отправление пользователя: {customer}");
            try
            {
                var stringContent = new StringContent(customer, Encoding.UTF8, "application/json");
                var response = await new HttpClient().PostAsync($"{ApiUrl}/user", stringContent);  
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error msg = {ex.Message}");
            }
        }
        static async void FindCustomer()
        {            
            try
            {
                Console.Write($"");
                Console.Write($"Поиск пользователя. Укажите ID: ");
                int.TryParse(Console.ReadLine(), out int customerId);
                var stringContent = new StringContent("", Encoding.UTF8, "application/json");
                var response = await new HttpClient().GetAsync($"{ApiUrl}/user/{customerId}");
                var resp = await response.Content.ReadAsStringAsync();
                Customer zzz = JsonConvert.DeserializeObject<Customer>(resp);
                Console.WriteLine($"Пользователь: {zzz}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error msg = {ex.Message}");
            }
        }
    }
}
