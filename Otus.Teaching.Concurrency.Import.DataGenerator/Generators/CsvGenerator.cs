﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CsvGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using var stream = File.CreateText(_fileName);

            foreach (var el in customers)
                stream.WriteLine(el);            
        }
        public async Task GenerateAsync()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            await using var stream = File.CreateText(_fileName);

            foreach (var el in customers)
                await stream.WriteLineAsync(el.ToString());
        }
    }
}
