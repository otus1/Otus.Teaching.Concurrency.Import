﻿using Otus.Teaching.Concurrency.Import.Handler.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class ManualJsonGenerator : IDataGenerator
    {
        private readonly int _customerId;
        private readonly string _fileName;
        public string ManualJsonCutomer { get; set; }

        public ManualJsonGenerator(int customerId, string fileName)
        {
            _customerId = customerId;
            _fileName = fileName;
        }
        public void Generate()
        {
            var customer = RandomCustomerGenerator.GenerateById(_customerId);
            ManualJsonCutomer = JsonSerializer.Serialize(customer);
        }

        public async Task GenerateAsync()
        {
            var customer = RandomCustomerGenerator.GenerateById(_customerId);
            ManualJsonCutomer = JsonSerializer.Serialize(customer);
        }

        public override string ToString()
        {
            return ManualJsonCutomer;
        }
    }
}

