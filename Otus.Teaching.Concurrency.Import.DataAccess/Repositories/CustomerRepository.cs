using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly DataContext _dataContext;

        public CustomerRepository(DataContext dataContext)
        {
            _dataContext = dataContext;            
        }

        public void AddCustomer(Customer customer)
        {
            _dataContext.Add(customer);
            _dataContext.SaveChanges();
        }
        public async Task AddCustomerAsync(Customer customer)
        {
            await _dataContext.AddAsync(customer);
            await _dataContext.SaveChangesAsync();
        }

        public void AddCustomers(List<Customer> customers)
        {
            _dataContext.Customers.AddRange(customers);
            _dataContext.SaveChanges();
        }
        public async Task AddCustomersAsync(List<Customer> customers)
        {
            await _dataContext.Customers.AddRangeAsync(customers);
            await _dataContext.SaveChangesAsync();
        }
        public void Dispose()
        {
            _dataContext?.Dispose();
        }

        public async Task<Customer> GetCustomerByIdAsync(int Id)
        {
            return await _dataContext.Customers.FirstOrDefaultAsync(x => x.Id == Id);
        }

        public async Task<bool> CheckCustomer(Customer customer)
        {
            var customerFind = await _dataContext.Customers.FirstOrDefaultAsync(x => x.Id == customer.Id);
            return customerFind != null;
        }

    }
}
