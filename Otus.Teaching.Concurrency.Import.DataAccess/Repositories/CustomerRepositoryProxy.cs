﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepositoryProxy : ICustomerRepository
    {        
        private IMemoryCache _memoryCache;
        private DataContext _dataContext;

        public CustomerRepositoryProxy(IMemoryCache memoryCache, DataContext dataContext)
        {
            _memoryCache = memoryCache;
            _dataContext = dataContext;
        }
        public void Dispose()
        {            
        }

        public void AddCustomer(Customer customer)
        {
            _dataContext.Add(customer);
            _dataContext.SaveChanges();
        }

        public async Task AddCustomerAsync(Customer customer)
        {
            await _dataContext.AddAsync(customer);
            int id = await _dataContext.SaveChangesAsync();
            if (id > 0)
            {
                _memoryCache.Set(customer.Id, customer, new MemoryCacheEntryOptions 
                { 
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(10), 
                    SlidingExpiration = TimeSpan.FromMinutes(3)
                });
            }
        }

        public async Task<bool> CheckCustomer(Customer customer)
        {
            var customerFind = await _dataContext.Customers.FirstOrDefaultAsync(x => x.Id == customer.Id);
            return customerFind != null;
        }

        public async Task<Customer> GetCustomerByIdAsync(int Id)
        {

            if (!_memoryCache.TryGetValue(Id, out Customer customer)) ;
            {
                customer = await _dataContext.Customers.FirstOrDefaultAsync(x => x.Id == Id);
                if (customer != null)
                {
                    _memoryCache.Set(customer.Id, customer, new MemoryCacheEntryOptions
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(10),
                        SlidingExpiration = TimeSpan.FromMinutes(3)
                    });
                }
            }
            return customer;

        }
    }
}
