﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        private string _path;

        public XmlParser(string path)
        {
            _path = path;
        }

        public List<Customer> Parse()
        {
            CustomersList customersList;
            using (Stream reader = new FileStream(_path, FileMode.Open))
            {
                var formater = new XmlSerializer(typeof(CustomersList));
                 customersList = (CustomersList)formater.Deserialize(reader);
            }
            return customersList?.Customers;

        }
    }
}