﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ServiceStack;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        private string _path;

        public CsvParser(string path)
        {
            _path = path;
        }

        public List<Customer> Parse()
        {
            CustomersList customersList = new CustomersList();
            customersList.Customers = new List<Customer>();
            var items = File.ReadAllText(_path).FromCsv<List<Customer>>();
            customersList.Customers.AddRange(items);
            return customersList?.Customers;
        }
    }
}
