﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ICustomerRepository _repository;

        public UserController(ICustomerRepository repository)
        {
            _repository = repository;
        }





        // GET: api/User/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            
            var customer = await _repository.GetCustomerByIdAsync(id);
            Console.WriteLine($"Resp = {customer.ToString()}");
            if (customer?.Id != null&& customer?.Id != 0)
                return Ok(customer);
            else
                return NotFound(Response);
        }

        // POST: api/User
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Customer customer)
        {
            try
            {
                var checkCustomer = await _repository.CheckCustomer(customer);
                if (!checkCustomer)
                {
                    await _repository.AddCustomerAsync(customer);
                    return Ok(customer);
                }
                return Conflict(Response);                
            }
            catch(Exception ex)
            {
                return BadRequest();
            }
        }

        
    }
}
