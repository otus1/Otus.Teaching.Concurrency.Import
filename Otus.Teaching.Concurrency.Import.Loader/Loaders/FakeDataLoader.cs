using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader
        : IDataLoader
    {
        public void LoadData()
        {
            Console.WriteLine("Loading data...");
            Thread.Sleep(10000);
            Console.WriteLine("Loaded data...");
        }

        public void LoadData(List<Customer> data)
        {
            throw new NotImplementedException();
        }

        public Task LoadDataAsync(List<Customer> data)
        {
            throw new NotImplementedException();
        }
    }
}