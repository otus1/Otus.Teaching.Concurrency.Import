﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class TreadPoolDataLoader : IDataLoader
    {
        private int _countThreads;
        private static DbContextOptions<DataContext> _DbOptions;

        public TreadPoolDataLoader(int countThreads, DbContextOptions<DataContext> DbOptions)
        {
            _countThreads = countThreads;
            _DbOptions = DbOptions;
        }

        public void LoadData(List<Customer> data)
        {            
            for (var i = 0; i < _countThreads; i++)
            {
                var stepCount = data.Count / _countThreads;
                var j = i;
                ThreadPool.QueueUserWorkItem(
                    new WaitCallback((s) =>
                    {
                        Load( data.Skip(j * stepCount).Take(stepCount).ToList());
                    }));
            }
           
        }
        private static void Load(IReadOnlyCollection<Customer> data)
        {
            var repo = new CustomerRepository(new DataContext(_DbOptions));
            repo.AddCustomers(data.ToList());
        }
        public async Task LoadDataAsync(List<Customer> data)
        {
            for (var i = 0; i < _countThreads; i++)
            {
                var stepCount = data.Count / _countThreads;
                var j = i;
                ThreadPool.QueueUserWorkItem(
                    new WaitCallback((s) =>
                    {
                        LoadAsync(data.Skip(j * stepCount).Take(stepCount).ToList());
                    }));
            }
        }
        private static async Task LoadAsync(IReadOnlyCollection<Customer> data)
        {
            var repo = new CustomerRepository(new DataContext(_DbOptions));
            await repo.AddCustomersAsync(data.ToList());
        }
    }
}
