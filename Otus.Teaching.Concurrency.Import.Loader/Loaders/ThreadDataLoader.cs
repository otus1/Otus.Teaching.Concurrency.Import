﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class ThreadDataLoader : IDataLoader
    {
        private int _countThreads;
        private DbContextOptions<DataContext> _DbOptions;

        public ThreadDataLoader(int countThreads, DbContextOptions<DataContext> DbOptions)
        {
            _countThreads = countThreads;
            _DbOptions = DbOptions;
        }


        public void LoadData(List<Customer> data)
        {
            List<Thread> threads = new List<Thread>();
            for (var i = 0; i < _countThreads; i++)
            {
                var stepCount = data.Count / _countThreads;
                var j = i;
                var thread = new Thread(() => Load(data.Skip(j * stepCount).Take(stepCount).ToList()));
                thread.Name = $"{thread}{j}";
                thread.Start();
                threads.Add(thread);
            }
            threads.ForEach(x => x.Join());
        }
        public void Load(IReadOnlyCollection<Customer> data)
        {
            var repo = new CustomerRepository(new DataContext(_DbOptions));
            repo.AddCustomers(data.ToList());
        }

        public async Task LoadDataAsync(List<Customer> data)
        {            
            for (var i = 0; i < _countThreads; i++)
            {
                double stepCountDbl = data.Count / _countThreads;
                int stepCount = Convert.ToInt32( Math.Floor(stepCountDbl)) +1;
                var j = i;
                var dataList = data.Skip(j * stepCount).Take(stepCount).ToList();
                await LoadAsync(dataList);             
            }
        }

        public async Task<bool> LoadAsync(IReadOnlyCollection<Customer> data)
        {
            using var dataContext = new DataContext(_DbOptions);
            var repo = new CustomerRepository(dataContext);
            await repo.AddCustomersAsync(data.ToList());
            return true;
        }
    }
}
