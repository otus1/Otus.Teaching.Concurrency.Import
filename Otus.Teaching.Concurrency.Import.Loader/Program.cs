﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using Otus.Teaching.Concurrency.Import.Loader.Model;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath;
        private static IDataLoader _dataLoader;
        private static int _countThread = 4;
        private static DbContextOptions<DataContext> DbOptions = null;

        static void Main(string[] args)
        {

            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }
            else
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("Settings.json")
                    .Build();

                var conf = builder.GetSection("DataGeneratorSettings").Get<DataGeneratorSettings>();              
                GenerateCustomersDataFile(conf);

                var connectionString = builder.GetConnectionString("CustomersDb");
                var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
                DbOptions = optionsBuilder.UseSqlite(connectionString).Options;
                ConfigureDataStorage();

            }
            
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            var parser = new XmlParser(_dataFilePath);
            //var parser = new CsvParser(_dataFilePath);
            var data = parser.Parse();
            
            var stopwatch = new Stopwatch();
            Console.WriteLine($"============================================");
            Console.WriteLine($"Start: {DateTime.Now.ToString("HH:mm:ss")}  Count:{data.Count};  CountThread: {_countThread}" );

            stopwatch.Start();
            _dataLoader = new ThreadDataLoader(_countThread, DbOptions);
            _dataLoader.LoadDataAsync(data);
            Console.WriteLine($"  {stopwatch.Elapsed} : {_dataLoader.GetType().Name} ");

            /*stopwatch.Start();
            _dataLoader = new TreadPoolDataLoader(_countThread, DbOptions);
            _dataLoader.LoadData(data);
            Console.WriteLine($"  {stopwatch.Elapsed} : {_dataLoader.GetType().Name} ");
            */
            Console.WriteLine($"============================================");           

        }

        static void GenerateCustomersDataFile( DataGeneratorSettings conf)
        {
            _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"{conf.Filename}.xml");

            if (conf.Enabled)
            {
                Process.Start(new ProcessStartInfo(conf.DataGeneratorProcessPath, $"{_dataFilePath} {conf.RecordCount}"));
                Thread.Sleep(3000);
            }
        }
        public static void ConfigureDataStorage()
        {
            using (DataContext context = new  DataContext(DbOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }
        }
    }
} 