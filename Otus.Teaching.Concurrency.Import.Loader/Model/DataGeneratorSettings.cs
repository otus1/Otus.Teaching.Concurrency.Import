﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader.Model
{
    public class DataGeneratorSettings
    {
        public bool Enabled { get; set; }
        public string DataGeneratorProcessPath { get; set; }
        public string Filename { get; set; }
        public int RecordCount { get; set; }
    }
}
