using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);
        Task AddCustomerAsync(Customer customer);
        Task<Customer> GetCustomerByIdAsync(int Id);
        Task<bool> CheckCustomer(Customer customer);
    }
}