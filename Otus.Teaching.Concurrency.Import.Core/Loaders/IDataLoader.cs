﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public interface IDataLoader
    {
        void LoadData(List<Customer> data);
        Task LoadDataAsync(List<Customer> data);
    }
}