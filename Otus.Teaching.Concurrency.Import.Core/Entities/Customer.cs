using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    public class Customer
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        public override string ToString()
        {
            return $"{Id},{FullName},{Email},{Phone}";
        }

        public Customer(int id, string fullName, string email, string phone)
        {
            Id = id;
            FullName = fullName;
            Email = email;
            Phone = phone;
        }
        public Customer()
        {

        }
    }
}